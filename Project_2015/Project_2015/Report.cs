﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using ExcelLibrary.CompoundDocumentFormat;
using ExcelLibrary.BinaryDrawingFormat;
using ExcelLibrary.BinaryFileFormat;
using ExcelLibrary.SpreadSheet;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms.DataVisualization.Charting;

namespace Project_2015
{
    public partial class Report : Form
    {

        //string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
        public Report()
        {
            InitializeComponent();  // loadreport();
           // loadtable();
        }

        /*
        void loadtable()
        {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select id,name,PW,userright,Firstname,Lastname,Team,Date from s4dugcom_test.user;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    
        }*/

       /* void TeamC()
        {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select Team,name,PW,userright,Firstname,Lastname,udate from s4dugcom_test.user where Team='Team C';";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        } */

      /*  void loadreport()
        {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select * from s4dugcom_test.task ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();



                while (myReader.Read())
                {
                    this.chart1.Series["Team A"].Points.AddXY(myReader.GetString("taskcontent"), myReader.GetInt32("tid"));
                   // this.chart1.Series["Team B"].Points.AddXY(myReader.GetString("PW"), myReader.GetInt32("id"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //  loadtable();
            myConn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadreport();
        }*/


      /*  //search data in table 
        private void textBox1_KeyUp(object sender, KeyEventArgs e)  //SELECT title FROM pages WHERE my_col LIKE %$param1% OR another_col LIKE %$param2%;
        {                                                                           //species = 'dog' OR species = 'cat';

           // string query = "select Team,userright,name,PW from s4dugcom_test.user where name like('" + textBox1.Text + "%\') or PW like('" + textBox1.Text + "%\') or Team like('" + textBox1.Text + "%\')  ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);
            myConn.Open();
           MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);

            DataTable dt = new DataTable();
           MySqlDataAdapter da = new MySqlDataAdapter(cmdDataBase);
            da.Fill(dt);
       //     dataGridView1.DataSource = dt;
        }
        //search data in table */



        private void button2_Click(object sender, EventArgs e)    //create PDF to folder  
        {
            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("Report2.pdf",FileMode.Create));
         // iTextSharp.text.Image PNG = iTextSharp.text.Image.GetInstance("pic.png");
           // PNG.ScalePercent(200f);

            doc.Open();
            /*
            

            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);

            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText));

            } table.HeaderRows = 1;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                for (int k = 0; k < dataGridView1.Columns.Count; k++)
                {
                    if(dataGridView1[k,i].Value!=null )
                    {
                table.AddCell(new Phrase(dataGridView1[k,i].Value.ToString()));
                    }
                }
            }
            doc.Add(table);  */

            var chartimage = new MemoryStream();
            chart1.SaveImage(chartimage, ChartImageFormat.Png);
            iTextSharp.text.Image Chart_image = iTextSharp.text.Image.GetInstance(chartimage.GetBuffer());
            doc.Add(Chart_image);

            var chartimage2 = new MemoryStream();
            chart2.SaveImage(chartimage2, ChartImageFormat.Png);
            iTextSharp.text.Image Chart_image2 = iTextSharp.text.Image.GetInstance(chartimage2.GetBuffer());
            doc.Add(Chart_image2);

            var chartimage3 = new MemoryStream();
            chart3.SaveImage(chartimage3, ChartImageFormat.Png);
            iTextSharp.text.Image Chart_image3 = iTextSharp.text.Image.GetInstance(chartimage3.GetBuffer());
            doc.Add(Chart_image3);

            var chartimage4 = new MemoryStream();
            chart4.SaveImage(chartimage4, ChartImageFormat.Png);
            iTextSharp.text.Image Chart_image4 = iTextSharp.text.Image.GetInstance(chartimage4.GetBuffer());
            doc.Add(Chart_image4);
            
            MessageBox.Show("Export Done","PDF");
            doc.Close();
            System.Diagnostics.Process.Start("explorer.exe", @"D:\Final_Project\Project_2015\Project_2015\bin\Debug"); 
        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

       

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
        //Get report details 
        private void button1_Click(object sender, EventArgs e)
        {
            this.chart1.Series["Client"].Points.AddXY("2013", 60);
            this.chart1.Series["Engineer"].Points.AddXY("2009", 15);

            this.chart1.Series["Client"].Points.AddXY("2014", 72);
            this.chart1.Series["Engineer"].Points.AddXY("2010", 21);

            this.chart1.Series["Client"].Points.AddXY("2015", 109);
            this.chart1.Series["Engineer"].Points.AddXY("2011", 33);



            this.chart2.Series["Team A"].Points.AddXY("Jan", 33);
            this.chart2.Series["Team B"].Points.AddXY("2014", 28);
            this.chart2.Series["Team C"].Points.AddXY("Jan", 20);
            this.chart2.Series["Team D"].Points.AddXY("2014", 28);
            this.chart2.Series["Team E"].Points.AddXY("Jan", 39);
            this.chart2.Series["Team F"].Points.AddXY("2014", 38);

            this.chart2.Series["Team A"].Points.AddXY("Feb", 45);
            this.chart2.Series["Team B"].Points.AddXY("2014", 32);
            this.chart2.Series["Team C"].Points.AddXY("Feb", 15);
            this.chart2.Series["Team D"].Points.AddXY("2014", 22); this.chart2.Series["Team E"].Points.AddXY("Feb", 35);
            this.chart2.Series["Team F"].Points.AddXY("2014", 32);

            this.chart2.Series["Team A"].Points.AddXY("Mar", 45);
            this.chart2.Series["Team B"].Points.AddXY("2014", 16);
            this.chart2.Series["Team C"].Points.AddXY("Jan", 29);
            this.chart2.Series["Team D"].Points.AddXY("2014", 18);
            this.chart2.Series["Team E"].Points.AddXY("Jan", 39);
            this.chart2.Series["Team F"].Points.AddXY("2014", 38);


            this.chart2.Series["Team A"].Points.AddXY("Apr", 47);
            this.chart2.Series["Team B"].Points.AddXY("2014", 30);
            this.chart2.Series["Team C"].Points.AddXY("Feb", 15);
            this.chart2.Series["Team D"].Points.AddXY("2014", 22); this.chart2.Series["Team E"].Points.AddXY("Feb", 35);
            this.chart2.Series["Team F"].Points.AddXY("2014", 12);


            this.chart2.Series["Team A"].Points.AddXY("May", 33);
            this.chart2.Series["Team B"].Points.AddXY("2014", 28);
            this.chart2.Series["Team C"].Points.AddXY("Jan", 19);
            this.chart2.Series["Team D"].Points.AddXY("2014", 8);
            this.chart2.Series["Team E"].Points.AddXY("Jan", 39);
            this.chart2.Series["Team F"].Points.AddXY("2014", 38);

            this.chart2.Series["Team A"].Points.AddXY("June", 33);
            this.chart2.Series["Team B"].Points.AddXY("2014", 28);
            this.chart2.Series["Team C"].Points.AddXY("Jan", 10);
            this.chart2.Series["Team D"].Points.AddXY("2014", 28);
            this.chart2.Series["Team E"].Points.AddXY("Jan", 29);
            this.chart2.Series["Team F"].Points.AddXY("2014", 38);

           this.chart3.Series["Services"].Points.AddXY("Jan", 33);

           this.chart3.Series["Services"].Points.AddXY("Feb", 130);

           this.chart3.Series["Services"].Points.AddXY("Mar", 100);

           this.chart3.Series["Services"].Points.AddXY("Apr", 150);

           this.chart3.Series["Services"].Points.AddXY("May", 160);

           this.chart3.Series["Services"].Points.AddXY("June", 155);


           this.chart4.Series["Server"].Points.AddXY("Jan", 13);
           this.chart4.Series["Server"].Points.AddXY("Feb", 20);
           this.chart4.Series["Server"].Points.AddXY("Mar", 20);
           this.chart4.Series["Server"].Points.AddXY("Apr", 13);
           this.chart4.Series["Server"].Points.AddXY("May", 20);
           this.chart4.Series["Server"].Points.AddXY("June", 20);

           this.chart4.Series["PC"].Points.AddXY("Jan", 20);
           this.chart4.Series["PC"].Points.AddXY("Feb", 30);
           this.chart4.Series["PC"].Points.AddXY("Mar", 33);
           this.chart4.Series["PC"].Points.AddXY("Apr", 22);
           this.chart4.Series["PC"].Points.AddXY("May", 31);
           this.chart4.Series["PC"].Points.AddXY("June", 30);

           this.chart4.Series["Network"].Points.AddXY("Jan", 5);
           this.chart4.Series["Network"].Points.AddXY("Feb", 6);
           this.chart4.Series["Network"].Points.AddXY("Mar", 3);
           this.chart4.Series["Network"].Points.AddXY("Apr", 5);
           this.chart4.Series["Network"].Points.AddXY("May", 6);
           this.chart4.Series["Network"].Points.AddXY("Jun", 3);


           this.chart4.Series["Installation"].Points.AddXY("Jan", 25);
           this.chart4.Series["Installation"].Points.AddXY("Feb", 15);
           this.chart4.Series["Installation"].Points.AddXY("Mar", 10);
           this.chart4.Series["Installation"].Points.AddXY("Apr", 21);
           this.chart4.Series["Installation"].Points.AddXY("May", 18);
           this.chart4.Series["Installation"].Points.AddXY("June", 15);

           this.chart4.Series["Others"].Points.AddXY("Jan", 15);
           this.chart4.Series["Others"].Points.AddXY("Feb", 9);
           this.chart4.Series["Others"].Points.AddXY("Mar", 11);
           this.chart4.Series["Others"].Points.AddXY("Apr", 16);
           this.chart4.Series["Others"].Points.AddXY("May", 11);
           this.chart4.Series["Others"].Points.AddXY("Jun", 15);

        }
        //Get report details 


        private void Report_FormClosing(object sender, FormClosingEventArgs e)
        {
          
        }

        private void button3_Click(object sender, EventArgs e) //Print functions get
        {
            PrintDialog dlg = new PrintDialog();
            dlg.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e) //Open email form
        {
            Mail m11 = new Mail();
            m11.Show();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
           
        }

        private void Report_Load(object sender, EventArgs e)
        {

        }

       

       
    }
}
        
        

    

            /*  string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            
            string query = "select * from s4dugcom_test.user ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();

                

                while (myReader.Read())
                {
                    this.chart1.Series["Team1"].Points.AddXY(myReader.GetString("name"),myReader.GetInt32("id"));
                    this.chart1.Series["Team2"].Points.AddXY(myReader.GetString("PW"), myReader.GetInt32("id"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          //  loadtable();
            myConn.Close();   
        } 
    }  */

