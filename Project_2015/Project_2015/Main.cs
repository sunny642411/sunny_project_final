﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using ExcelLibrary.CompoundDocumentFormat;
using ExcelLibrary.SpreadSheet;
using ExcelLibrary.BinaryFileFormat;
using ExcelLibrary.BinaryDrawingFormat;

namespace Project_2015
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
          //  Fillcombo(); //load combo fill it to combobox1
            Filelistbox(); //load list from DB
            loadtable();// load datatable once start up

            textBox2.PasswordChar = '#';
            textBox2.MaxLength = 8;
            textBox1.MaxLength = 8; 
            
        }

       // string Checkbox;
        string userright;
        //Fill name from database to combo 
        void Filelistbox() {

            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //  string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select* from s4dugcom_test.user ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();



                while (myReader.Read())
                {

                    string sName = myReader.GetString("name");
                    listBox1.Items.Add(sName);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        void Fillcombo() {

            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            
          //  string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select* from s4dugcom_test.user ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();

              

                while (myReader.Read())
                {

                    string sName = myReader.GetString("name");
                    comboBox1.Items.Add(sName);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        
        }
       
     

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e) //save all input data to my sql database 
        {

         //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
           // string query = "Insert into s4dugcom_test.user (id,name,PW,userright,Firstname,Lastname) values('" + textBox3.Text + "','" + textBox1.Text + "','" + textBox2.Text + "','" + userright + "','" + textBox5.Text + "','" + textBox4.Text + "');"; 

            string query = "Insert into s4dugcom_test.user (id,name,PW,userright,staffname,Team,Status,udate) values('" + textBox3.Text + "','" + textBox1.Text + "','" + textBox2.Text + "','" + userright + "','" + textBox5.Text + "','" + comboBox1.SelectedItem.ToString() + "','" + comboBox2.SelectedItem.ToString() + "','"+dateTimePicker1.Text+"');";
            
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("Saved");
              

                while (myReader.Read())
                {
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            loadtable();
          //  Filelistbox();
            myConn.Close();

        }

        private void button2_Click(object sender, EventArgs e) //update data to mydsql
        {

          //  string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            string query = "update s4dugcom_test.user set id='" + this.textBox3.Text + "' ,name='" + this.textBox1.Text + "',PW='" + this.textBox2.Text + "',userright='" + userright + "',staffname='" + textBox5.Text + "',Team='" + comboBox1.SelectedItem.ToString() + "',Status='" + comboBox2.SelectedItem.ToString() + "', udate='" + dateTimePicker1.Text + "' where id='" + this.textBox3.Text + "';";
           // string query = "update s4dugcom_test.user set id='" + this.textBox3.Text + "' ,name='" + this.textBox1.Text + "',PW='" + this.textBox2.Text + "' where id='" + this.textBox3.Text+"';";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();

                MessageBox.Show("Update");

                while (myReader.Read())
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            loadtable();
           // Filelistbox();
            myConn.Close();
        }

        private void button3_Click(object sender, EventArgs e)    // Delete all select record from my sql database
        {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";
            
           // string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "delete from s4dugcom_test.user where id='" + this.textBox3.Text + "';";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();

                MessageBox.Show("Deleted");
             
                while (myReader.Read())
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            loadtable();

            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
        //    textBox4.Clear();
            textBox5.Clear();
         //   Filelistbox();
            myConn.Close();

        }

       
       


        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            userright = "Viewonly";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            userright = "Full";
        }

        //data table load

        void loadtable() {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select * from s4dugcom_test.user;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);


              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void button5_Click(object sender, EventArgs e) //export all data to excel file
        {
           // loadtable();
           string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select id,name,PW,userright from s4dugcom_test.user;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = cmdDataBase;
                DataTable dbdataset = new DataTable();
               // sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);

                DataSet ds = new DataSet("New_DataSet");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                sda.Fill(dbdataset);
                ds.Tables.Add(dbdataset);
                ExcelLibrary.DataSetHelper.CreateWorkbook("My.xls", ds);
              
              //  System.Diagnostics.Process.Start("My.xls");
                MessageBox.Show("File Exported","Message");
          
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            
            
        }

        

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) //select each name in the list box will display related information to textbox
        {
            string myconnection = "datasource= 202.67.231.247 ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            //   string myconnection = "datasource=s4.duscomputing.com ;port=3306; username=s4dugcom_sunny; password=Pa$$w0rd";

            string query = "select* from s4dugcom_test.user where name='" + listBox1.Text + "'  ;";
            MySqlConnection myConn = new MySqlConnection(myconnection);

            MySqlCommand cmdDataBase = new MySqlCommand(query, myConn);


            MySqlDataReader myReader;
            try
            {
                myConn.Open();

                myReader = cmdDataBase.ExecuteReader();



                while (myReader.Read())
                {

                    string sName = myReader.GetString("name");
                    string sPW = myReader.GetString("PW");

                    string stus = myReader.GetString("status");
                    string sname = myReader.GetString("staffname");
                    string sid = myReader.GetInt32("id").ToString(); string udate1 = myReader.GetString("udate");
                   textBox3.Text = sid;
                    textBox1.Text = sName;
                    textBox2.Text = sPW;
                    comboBox2.Text = stus;
textBox5.Text = sname;
dateTimePicker1.Text = udate1;

                  //  MessageBox.Show(sid);
                    // string sName = myReader.GetString("name");
                    // string sName = myReader.GetString("name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e) // exit the application
        {
            DialogResult dialog = MessageBox.Show("Do you want to exit?", "Exit", MessageBoxButtons.YesNo);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
            else if (dialog == DialogResult.No)
            {
               // e.Cancel = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

     /*   private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)  // Select DataTable and select it show in textbox
        {
           if(e.RowIndex>=0){    
            DataGridView1Row row=this.dataGridView1.Rows[e.RowIndex];

                textBox3.Text=row.Cells["id"].Value.ToString();
                textBox1.Text=row.Cells["name"].Value.ToString();
                textBox2.Text=row.Cells["PW"].Value.ToString();
             
           

              
            }
        }*/

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  MessageBox.Show(comboBox1.SelectedItem.ToString());

        }

     
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click_1(object sender, EventArgs e) //clear all input data from all textbox exiting
        {

            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
          //  textBox4.Clear();
            textBox5.Clear();
            radioButton1.Checked = false;
            radioButton2.Checked = false;

            radioButton3.Checked = false;
          /*  comboBox4.Text = "";
            comboBox5.Text = ""; comboBox6.Text = "";
            comboBox1.Text = "Start Up";
            checkBox1.Checked = false;
            checkBox2.Checked = false; */
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >=0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];

                textBox1.Text = row.Cells["name"].Value.ToString();



            }




        }

        private void textBox6_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
      
    }
}
             
    

    

